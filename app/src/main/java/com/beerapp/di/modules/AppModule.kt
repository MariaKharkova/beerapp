package com.beerapp.di.modules

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.beerapp.data.local.AppPreferences
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [NetworkModule::class, ViewModelsModule::class, ViewModelFactoryModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideAppPreferences(sharedPreferences: SharedPreferences): AppPreferences {
        return AppPreferences(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(application: Application): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)

    @Provides
    @Singleton
    fun providePicasso(application: Application): Picasso {
        val builder = Picasso.Builder(application)
        return builder.build()
    }
}