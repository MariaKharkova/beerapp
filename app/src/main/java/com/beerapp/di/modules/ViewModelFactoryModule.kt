package com.beerapp.di.modules

import android.arch.lifecycle.ViewModelProvider
import com.beerapp.ui.common.BeerViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: BeerViewModelFactory): ViewModelProvider.Factory
}