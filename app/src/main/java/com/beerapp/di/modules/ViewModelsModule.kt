package com.beerapp.di.modules

import android.arch.lifecycle.ViewModel
import com.beerapp.di.ViewModelKey
import com.beerapp.ui.details.DetailsViewModel
import com.beerapp.ui.favorite.FavoritesBeerViewModel
import com.beerapp.ui.listAll.AllBeerViewModel
import com.beerapp.ui.random.RandomViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(RandomViewModel::class)
    abstract fun bindRandomViewModel(randomViewModel: RandomViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AllBeerViewModel::class)
    abstract fun bindAllBeersViewModel(allBeersViewModel: AllBeerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindDetailsViewModel(detailsViewModel: DetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavoritesBeerViewModel::class)
    abstract fun bindFavoritesBeerViewModel(favoritesBeerViewModel: FavoritesBeerViewModel): ViewModel
}