package com.beerapp.di.modules

import com.beerapp.ui.details.DetailsActivity
import com.beerapp.ui.favorite.FavoriteBeersFragment
import com.beerapp.ui.listAll.AllBeersFragment
import com.beerapp.ui.random.RandomFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeRandomFragment(): RandomFragment

    @ContributesAndroidInjector
    abstract fun contributeAllBeersFragment(): AllBeersFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoriteBeersFragment(): FavoriteBeersFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailsActivity(): DetailsActivity
}
