package com.beerapp.di

import com.beerapp.BeerApp

object AppInjector {
    fun init(beerApp: BeerApp) {
        DaggerAppComponent.builder().application(beerApp)
            .build().inject(beerApp)
    }
}