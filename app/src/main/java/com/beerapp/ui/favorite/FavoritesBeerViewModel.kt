package com.beerapp.ui.favorite

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.beerapp.domain.models.Beer
import com.beerapp.domain.models.wrappers.Resource
import com.beerapp.domain.repositories.BeerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FavoritesBeerViewModel @Inject constructor(private val beerRepository: BeerRepository) : ViewModel() {
    private var disposable: CompositeDisposable? = CompositeDisposable()
    private val beerList = MutableLiveData<Resource<List<Beer>>>()

    init {
        fetchFavoriteBeerList()
    }

    private fun fetchFavoriteBeerList() {
        beerList.value = Resource.loading()
        disposable?.add(
            beerRepository.getFavoriteBeerList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Beer>>() {
                    override fun onSuccess(list: List<Beer>) {
                        beerList.value = Resource.success(list)
                    }

                    override fun onError(e: Throwable) {
                        beerList.value = Resource.error(e.message)
                    }
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.clear()
        disposable = null
    }

    internal fun getFavoriteBeerList(): MutableLiveData<Resource<List<Beer>>> {
        return beerList
    }

    internal fun removeFromFavorite(beer: Beer) {
        return beerRepository.removeFromFavorite(beer.id)
    }
}

