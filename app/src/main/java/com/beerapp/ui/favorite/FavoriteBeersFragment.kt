package com.beerapp.ui.favorite

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.beerapp.domain.models.Beer
import com.beerapp.domain.models.wrappers.Resource
import com.beerapp.domain.models.wrappers.Status
import com.beerapp.ui.common.OnItemSwipedListener
import com.beerapp.ui.common.SwipeToDeleteCallback
import com.beerapp.ui.details.DetailsActivity
import com.beerapp.ui.listAll.BeerActionListener
import com.beerapp.ui.listAll.BeerAdapter
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.favorite_beers_fragment.*
import javax.inject.Inject


class FavoriteBeersFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: FavoritesBeerViewModel
    private lateinit var adapter: BeerAdapter
    @Inject
    lateinit var picasso: Picasso

    private val beerListObserver = Observer<Resource<List<Beer>>> { response ->
        response?.let {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { list -> adapter.updateListData(list = list as ArrayList<Beer>) }
                    progressFavorite.visibility = View.GONE
                }
                Status.ERROR -> {
                    Toast.makeText(context, it.errorMessage, Toast.LENGTH_SHORT).show()
                    progressFavorite.visibility = View.GONE
                }
                Status.LOADING -> progressFavorite.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(com.beerapp.R.layout.favorite_beers_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(FavoritesBeerViewModel::class.java)
        viewModel.getFavoriteBeerList().observe(this, beerListObserver)
        initBeerList()
    }

    private fun initBeerList() {
        adapter = BeerAdapter(context = context!!, listener = object : BeerActionListener {
            override fun onBeerSelected(beer: Beer) {
                startActivity(DetailsActivity.newIntent(activity!!, beer.id))
            }
        }, picasso = picasso)
        favoriteBeerList.adapter = adapter
        favoriteBeerList.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(object : OnItemSwipedListener {
            override fun onItemSwiped(position: Int) {
                viewModel.removeFromFavorite(adapter.deleteItem(position))
            }
        }))

        itemTouchHelper.attachToRecyclerView(favoriteBeerList)
    }

}
