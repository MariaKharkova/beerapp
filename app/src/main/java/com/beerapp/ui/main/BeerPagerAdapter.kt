package com.beerapp.ui.main

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.beerapp.ui.favorite.FavoriteBeersFragment
import com.beerapp.ui.listAll.AllBeersFragment
import com.beerapp.ui.random.RandomFragment

private const val allTabPosition = 0
private const val randomTabPosition = 1
private const val favoriteFragmentPosition = 2

class BeerPagerAdapter(fragmentManager: FragmentManager, private var tabsAmount: Int) :
    FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment? {
        return when (position) {
            allTabPosition -> {
                AllBeersFragment()
            }
            randomTabPosition -> {
                RandomFragment()
            }
            favoriteFragmentPosition -> {
                FavoriteBeersFragment()
            }
            else -> null
        }
    }

    override fun getCount(): Int {
        return tabsAmount
    }
}