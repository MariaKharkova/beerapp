package com.beerapp.ui.main

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import com.beerapp.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpTabs()
    }

    private fun setUpTabs() {
        tabs.addTab(tabs.newTab().setText(getString(R.string.all)))
        tabs.addTab(tabs.newTab().setText(getString(R.string.random)))
        tabs.addTab(tabs.newTab().setText(getString(R.string.favorite)))
        tabs.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = BeerPagerAdapter(supportFragmentManager, tabs.tabCount)
        beerPager.adapter = adapter
        beerPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {}

            override fun onTabUnselected(p0: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab) {
                beerPager.currentItem = tab.position
            }
        })
    }
}
