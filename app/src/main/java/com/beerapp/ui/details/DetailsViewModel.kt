package com.beerapp.ui.details

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.beerapp.domain.models.Beer
import com.beerapp.domain.models.wrappers.Resource
import com.beerapp.domain.repositories.BeerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailsViewModel @Inject constructor(private val beerRepository: BeerRepository) : ViewModel() {
    private var disposable: CompositeDisposable? = CompositeDisposable()
    private val beerDetails = MutableLiveData<Resource<Beer>>()
    private val isInFavorite = MutableLiveData<Boolean>()
    private var beerId: Long = -1L


    fun getBeerDetails(id: Long): MutableLiveData<Resource<Beer>> {
        beerId = id
        beerDetails.value = Resource.loading()
        isInFavorite.value = beerRepository.isBeerInFavorite(id)
        disposable?.add(
            beerRepository.getBeerDetails(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Beer>>() {
                    override fun onSuccess(list: List<Beer>) {
                        beerDetails.value = Resource.success(list[0])
                    }

                    override fun onError(e: Throwable) {
                        beerDetails.value = Resource.error(e.message)
                    }
                })
        )
        return beerDetails
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.clear()
        disposable = null
    }


    fun processFavoriteAction() {
        if (beerId != -1L) {
            isInFavorite.value = beerRepository.tryAddToFavorite(beerId)
        }
    }

    internal fun isInFavorite(): MutableLiveData<Boolean> {
        return isInFavorite
    }
}