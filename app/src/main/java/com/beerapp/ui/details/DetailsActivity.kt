package com.beerapp.ui.details

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.beerapp.R
import com.beerapp.domain.models.Beer
import com.beerapp.domain.models.wrappers.Resource
import com.beerapp.domain.models.wrappers.Status
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject

class DetailsActivity : AppCompatActivity() {

    companion object {

        private const val INTENT_SELECTED_BEER_ID = "INTENT_SELECTED_BEER_ID"

        fun newIntent(context: Context, beerId: Long): Intent {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(INTENT_SELECTED_BEER_ID, beerId)
            return intent
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: DetailsViewModel

    private val beerObserver = Observer<Resource<Beer>> { response ->
        response?.let {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { it1 -> beerDetails.setUpBeerInfo(it1) }
                }
                Status.ERROR -> {
                    beerDetails.clear()
                    Toast.makeText(this, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> Toast.makeText(this, getString(R.string.loading), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val inFavoriteObserver = Observer<Boolean> { response ->
        response?.let { inFavorite ->
            toFavorite.setImageResource(if (inFavorite) R.drawable.ic_favorite_selected else R.drawable.ic_favorite)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailsViewModel::class.java)
        viewModel.getBeerDetails(intent.getLongExtra(INTENT_SELECTED_BEER_ID, 1)).observe(this, beerObserver)
        viewModel.isInFavorite().observe(this, inFavoriteObserver)
        toFavorite.setOnClickListener { view -> viewModel.processFavoriteAction() }
    }
}
