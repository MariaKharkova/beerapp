package com.beerapp.ui.common

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.beerapp.R
import com.beerapp.domain.models.Beer
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.beer_details_layout.view.*

class BeerDetailsLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = -1) : ConstraintLayout(context, attrs, defStyleAttr){

    private val picasso = Picasso.Builder(context).build()
    init {
        LayoutInflater.from(context)
            .inflate(R.layout.beer_details_layout, this, true)
    }

    fun setUpBeerInfo(beer: Beer){
        initFields(beer)
    }

    fun clear(){
        initFields(null)
    }

    private fun initFields(beer: Beer?){
        picasso.load(beer?.imageUrl)
            .placeholder(R.drawable.ic_placeholder)
            .into(beerImage)
        beerName.text = beer?.name ?:""
        beerDescription.text =  beer?.description?:""
    }
}