package com.beerapp.ui.listAll

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.beerapp.domain.models.Beer
import com.beerapp.domain.models.wrappers.Resource
import com.beerapp.domain.repositories.BeerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class AllBeerViewModel @Inject constructor(private val beerRepository: BeerRepository) : ViewModel() {
    private var disposable: CompositeDisposable? = CompositeDisposable()
    private val beerList = MutableLiveData<Resource<List<Beer>>>()
    private val beerData = ArrayList<Beer>()
    private var currentPage: Int = 1
    private var searchQuery: String? = null
    private var isLoading = false

    init {
        fetchBeerList(currentPage,searchQuery)
    }

    fun search(query: String){
        currentPage = 1
        searchQuery = if (query.isEmpty()) null else query
        fetchBeerList(currentPage, searchQuery)
    }

    fun loadMore(){
        fetchBeerList(page=++currentPage, query = searchQuery)
    }

    private fun fetchBeerList(page: Int, query: String?) {
        if (!isLoading) {
            isLoading = true
            beerList.value = Resource.loading()
            disposable?.add(
                beerRepository.getBeerList(page, query).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : DisposableSingleObserver<List<Beer>>() {
                        override fun onSuccess(list: List<Beer>) {
                            if (currentPage == 1) {
                                beerData.clear()
                            }
                            beerData.addAll(list)
                            beerList.value = Resource.success(beerData)
                            isLoading = false
                        }

                        override fun onError(e: Throwable) {
                            beerList.value = Resource.error(e.message)
                            isLoading = false
                        }
                    })
            )
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.clear()
        disposable = null
    }

    internal fun getBeerList(): MutableLiveData<Resource<List<Beer>>> {
        return beerList
    }
}
