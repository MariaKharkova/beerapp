package com.beerapp.ui.listAll

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.beerapp.domain.models.Beer
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_beer_layout.view.*


private const val VIEW_TYPE_ITEM = 0
private const val VIEW_TYPE_LOADING = 1

class BeerAdapter(
    private var beerList: ArrayList<Beer> = ArrayList(),
    context: Context,
    private val listener: BeerActionListener,
    private val picasso: Picasso
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var hasExtraRow = false


    override fun getItemCount(): Int {
        return beerList.size + getExtraRow()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_ITEM) {
            BeerViewHolder(
                LayoutInflater.from(parent.context).inflate(com.beerapp.R.layout.item_beer_layout, parent, false),
                listener = listener
            )
        } else {
            LoadingViewHolder(
                LayoutInflater.from(parent.context).inflate(com.beerapp.R.layout.item_loading, parent, false)
            )
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder.itemViewType == VIEW_TYPE_ITEM) {
            (viewHolder as BeerViewHolder).bind(beerList[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow && position == itemCount - 1) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    fun deleteItem(position: Int): Beer {
        val deletedItem = beerList[position]
        beerList.removeAt(position)
        notifyItemRemoved(position)
        return deletedItem
    }

    fun updateListData(list: ArrayList<Beer>) {
        if (hasExtraRow) {
            hasExtraRow = false
            notifyItemRemoved(itemCount)
        }
        this.beerList = list
        notifyDataSetChanged()
    }

    private fun getExtraRow(): Int {
        return if (hasExtraRow)
            1
        else
            0
    }

    fun showLoading() {
        hasExtraRow = true
        notifyItemInserted(itemCount + 1)
    }

    private inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class BeerViewHolder(private val view: View, val listener: BeerActionListener) :
        RecyclerView.ViewHolder(view) {

        fun bind(beer: Beer) {
            itemView.setOnClickListener { listener.onBeerSelected(beer) }
            view.beerName.text = beer.name
            picasso.load(beer.imageUrl).placeholder(com.beerapp.R.drawable.ic_placeholder).into(view.beerIcon)
        }
    }

}

interface BeerActionListener {
    fun onBeerSelected(beer: Beer)
}