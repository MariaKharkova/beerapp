package com.beerapp.ui.listAll

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import com.beerapp.domain.models.Beer
import com.beerapp.domain.models.wrappers.Resource
import com.beerapp.domain.models.wrappers.Status
import com.beerapp.ui.common.EndlessRecyclerOnScrollListener
import com.beerapp.ui.details.DetailsActivity
import com.beerapp.ui.utils.hideKeyBoard
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.all_beers_fragment.*
import javax.inject.Inject


class AllBeersFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AllBeerViewModel
    private lateinit var adapter: BeerAdapter
    @Inject
    lateinit var picasso: Picasso

    private val beerListObserver = Observer<Resource<List<Beer>>> { response ->
        response?.let {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { list -> adapter.updateListData(list = list as ArrayList<Beer>) }
                }
                Status.ERROR -> {
                    Toast.makeText(context, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
                Status.LOADING ->   adapter.showLoading()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(com.beerapp.R.layout.all_beers_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AllBeerViewModel::class.java)
        viewModel.getBeerList().observe(this, beerListObserver)
        setupViews()
    }

    private fun setupViews() {
        initBeerList()
        setUpSearchField()
    }

    private fun initBeerList(){
        adapter = BeerAdapter( context = context!! , listener = object:BeerActionListener{
            override fun onBeerSelected(beer: Beer) {
                startActivity(DetailsActivity.newIntent(activity!!, beer.id))
            }

        }, picasso = picasso)
        beerList.adapter = adapter
        beerList.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        initScrollListener()
    }

    private fun setUpSearchField() {
        searchBeer.setOnEditorActionListener { view, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                viewModel.search(view.text.toString())
                context?.hideKeyBoard(view)
                true
            } else {
                false
            }
        }
    }


    private fun initScrollListener() {
        beerList.addOnScrollListener(object :EndlessRecyclerOnScrollListener(beerList.layoutManager as LinearLayoutManager){
            override fun onLoadMore() {
                viewModel.loadMore()
            }

        })
    }



}
