package com.beerapp.ui.random

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.beerapp.R
import com.beerapp.domain.models.Beer
import com.beerapp.domain.models.wrappers.Resource
import com.beerapp.domain.models.wrappers.Status
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.random_fragment.*
import javax.inject.Inject


class RandomFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RandomViewModel

    private val beerObserver = Observer<Resource<Beer>> { response ->
        response?.let {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { it1 -> beerDetails.setUpBeerInfo(it1) }
                }
                Status.ERROR -> {
                    beerDetails.clear()
                    Toast.makeText(context, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
                Status.LOADING -> Toast.makeText(context, getString(R.string.loading), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.random_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RandomViewModel::class.java)
        setUpRandomButton()
        viewModel.getRandomBeer().observe(this, beerObserver)
    }

    private fun setUpRandomButton() {
        randomBtn.setOnClickListener { view -> viewModel.generateRandomBeer() }
    }

}
