package com.beerapp.ui.random

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.beerapp.domain.models.Beer
import com.beerapp.domain.models.wrappers.Resource
import com.beerapp.domain.repositories.BeerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RandomViewModel @Inject constructor(private val beerRepository: BeerRepository) : ViewModel() {
    private var disposable: CompositeDisposable? = CompositeDisposable()
    private val randomBeer = MutableLiveData<Resource<Beer>>()

    internal fun generateRandomBeer() {
        randomBeer.value = Resource.loading()
        disposable?.add(
            beerRepository.getBeerRandom().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Beer>>() {
                    override fun onSuccess(beerList: List<Beer>) {
                        randomBeer.value = Resource.success(beerList[0])
                    }

                    override fun onError(e: Throwable) {
                        randomBeer.value = Resource.error(e.message)
                    }
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.clear()
        disposable = null
    }

    internal fun getRandomBeer(): MutableLiveData<Resource<Beer>> {
        return randomBeer
    }
}
