package com.beerapp.domain.repositories

import com.beerapp.data.api.ApiInterface
import com.beerapp.data.local.AppPreferences
import com.beerapp.domain.models.Beer
import io.reactivex.Single
import javax.inject.Inject

class BeerRepository @Inject constructor(
    private val apiInterface: ApiInterface,
    private val appPreferences: AppPreferences
) {

    fun getBeerList(page: Int, query: String?): Single<List<Beer>> {
        return apiInterface.getBeerList(page = page, beerName = query)
    }

    fun getBeerRandom(): Single<List<Beer>> {
        return apiInterface.getRandomBeer()
    }

    fun getBeerDetails(id: Long): Single<List<Beer>> {
        return apiInterface.getBeerById(id)
    }

    fun getFavoriteBeerList(): Single<List<Beer>> {
        val ids = appPreferences.getFavorites().joinToString(separator = "|")
        if (ids.isNotEmpty()) {
            return apiInterface.getFavoriteBeerList(ids)
        }
        return Single.just(arrayListOf())
    }

    fun isBeerInFavorite(id: Long): Boolean {
        return appPreferences.getFavorites().contains(id)
    }

    fun tryAddToFavorite(id: Long): Boolean {
        val isInFavorite = isBeerInFavorite(id)
        if (!isBeerInFavorite(id)) {
            appPreferences.addToFavorite(id)
        } else {
            removeFromFavorite(id)
        }
        return !isInFavorite
    }

    fun removeFromFavorite(id: Long) {
        appPreferences.removeFromFavorite(id)
    }
}