package com.beerapp.domain.models

import com.squareup.moshi.Json

data class Beer(
    val id: Long,
    val name: String,
    val description: String?,
    @Json(name = "image_url")
    val imageUrl: String?
)