package com.beerapp.data.local

import android.content.SharedPreferences
import javax.inject.Inject

private const val PREFERENCES_FAVORITE_BEER_IDS = "PREFERENCES_FAVORITE_BEER_IDS"

class AppPreferences @Inject constructor(private val preferences: SharedPreferences) {

    fun addToFavorite(id: Long) {
        val newSet = HashSet<String>(preferences.getStringSet(PREFERENCES_FAVORITE_BEER_IDS, mutableSetOf()) as MutableSet<String>)
        newSet.add(id.toString())
        preferences.edit().putStringSet(PREFERENCES_FAVORITE_BEER_IDS, newSet).apply()
    }

    fun removeFromFavorite(id: Long) {
        val newSet = HashSet<String>(preferences.getStringSet(PREFERENCES_FAVORITE_BEER_IDS, mutableSetOf()) as MutableSet<String>)
        newSet.remove(id.toString())
        preferences.edit().putStringSet(PREFERENCES_FAVORITE_BEER_IDS, newSet).apply()
    }

    fun getFavorites(): List<Long>{
        return (preferences.getStringSet(PREFERENCES_FAVORITE_BEER_IDS, mutableSetOf()) as MutableSet<String>).map { it.toLong() }
    }
}