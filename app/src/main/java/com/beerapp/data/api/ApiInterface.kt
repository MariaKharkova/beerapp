package com.beerapp.data.api

import com.beerapp.domain.models.Beer
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    @GET("v2/beers")
    fun getBeerList(@Query("page") page: Int, @Query("beer_name", encoded = true) beerName: String?): Single<List<Beer>>

    @GET("v2/beers/random")
    fun getRandomBeer(): Single<List<Beer>>

    @GET("v2/beers/{id}")
    fun getBeerById(@Path("id") id: Long): Single<List<Beer>>

    @GET("v2/beers")
    fun getFavoriteBeerList(@Query("ids", encoded = true) ids: String): Single<List<Beer>>
}